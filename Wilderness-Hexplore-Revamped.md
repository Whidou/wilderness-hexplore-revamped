﻿---
title: Wilderness Hexplore revamped
author: Jed McClure
editors:
 - MamaDM
 - Whidou
 - Wanderer Bill
cheerleader: presgas ;-) 
licence: OGL 1.0a
---


Wilderness Exploration
======================

These tables describe how to run an improvised wilderness hex crawl. The idea is that the even the GM does not know what the players will encounter, since the map, features, and encounters are all rolled randomly at the table. Every time the players explore a new hex, there are three primary rolls that need to be made.  It works well to assign particular players to be responsible for some of these rolls to speed play- and also keep the players a part of the world creation. However the GM should always be the one to make the encounter roll. The party should have a blank Hex Region map (Make copies of the one at the end of the document) and a ‘starting point’ hex. If rolling for the starting hex, roll a d8 for the row and then the column. Next roll or select a hex terrain type for their starting point- this will be important for when they begin exploring the hexes around the starting hex. (You may want to roll up a village for their starting hex using the village and town rules. This can be their safe place for buying rations and selling treasure.)

Basic procedure
---------------
Every time the PCs explore a new adjacent hex, these three roll are made first:

- Determine the Hex Terrain type - using the table on the next page, roll a D20 and cross reference it with the hex type they just exited. This tells the type of the new hex. (There is a 50% chance that it is the same type as before)
- Roll for presence of a Feature - A d8 roll to see if the party finds something of interest in that hex. Most of this document are the tables for determining the exact nature of a feature that is found.
- Roll for Encounter - A d12 roll that answers if they encountered anyone or anything while they were exploring the hex. The encounter can happen before, after or during the discovery of a feature, (GM’s choice)

Frequently a new hex’s terrain will be of the same terrain type as the previous, and the party will not have any encounters or find any features. This is normal. For each hex (whether or not something is found) the party should decide if they want to move on in the same direction, or pick a new direction. 

The GM should note the passage of time spent in each hex, before rolling the next hex.The GM (or better yet a designated player) should record the terrain type, and whatever feature they may have found in the region map annotations. Encounters should be rolled every time a hex is entered, even if it has been previously explored.  If they party wants to spend more time exploring a hex, the GM can allow they to make another feature roll to see if they found something they missed the first time, but additional encounter rolls should also be made.

Note: be sure to track the passage of time, and the exhaustion of rations and resources. Wilderness hexcrawls are like a megadungeon, in that they are a type of challenge that requires the party to see how far they can push the odds before returning to town. If there is no penalty for exploring non-stop, then the hexcrawl will soon stop being fun for the players.

Travel time
-----------
Crossing/exploring a hex takes a certain amount time, based on its Difficulty, usually either 3 or 4 hours.  There are about 15-18 hours in the day that a party can explore before they need to camp for the night.  A party that explores for MORE then 18 hours will suffer a -1 to hit rolls the following day, unless they spend extra time the following day resting. Thus a party can generally explore 5 Moderate or 4 Difficult hexes in a single day, without penalty,

Food
----
Each PC and hireling eats one days rations each day.  For each day a party member does not get a full ration, their ‘to hit’ roll gets a -1 modifier. This is cumulative for each day they are not able to be properly fed.Thus mark off 1 days rations for each character and hireling for after 4 or 5 hexes explored.

If the party runs out of stores, they have the following options:

 - Foraging: The party as a whole may roll 1d6 once per day of exploration, and on the result of a ‘1’ they found enough food to feed 1d6 party members for the day. (Note: they must state they are foraging at the start of the day.)
 - Hunting: If the party chooses they may spend a day hunting- this is the only activity the party can make that day, and they cannot explore any new hexes.  Each party member who chooses to hunt, can roll 1d6, and on the result of 1 or 2 they caught enough game for 1d6 party members. 
 - Ranger and Druid characters may deserve a bonus to these rolls, depending on the environment. 
 - Hunting or foraging in particularly bleak environments may require a negative modifier to the roll, at the GM’s discretion.

Terrain
-------

**First Roll: Determine the terrain**
Roll a d20 to determine how the new hex changes.  Tables for climates other then temperate are given page Wilderness Exploration-6

TEMPERATE
Mid latitudes similar to western Europe:

|  1d20 | PLAINS | FOREST | MARSH | HILLS | MOUNTAINS | WATER |
|:-----:|--------|--------|-------|-------|-----------|-------|
|  1-10 | Plains | Forest | Marsh | Hills | Mountains | Water |
| 11-16 | Forest | Plains | Water | Mountains | Hills | Marsh |
| 17-18 | Marsh | Hills | Forest | Water | Plains | Mountain |
|    19 | Mountains | Mountains | Plains | Forest | Forest | Plains |
|    20 | Water | Marsh | Hills | Marsh | Water | Hills |

Feature
-------

**Second Roll: Roll to see if there is a feature in the hex**

**Feature Type Roll**

Encounters
----------

### THIRD ROLL: RANDOM ENCOUNTERS 

The following 2d8 random encounter tables are adapted from the *Basic Fantasy RPG* by Chris Gonnermann and are open game content.

2d8 | Desert or Barren | Grassland | Inhabited Territories
:-------:|------------------|-----------|-----------------------|
2 | Dragon, Blue | Dragon, Green | Dragon, Gold
3 | Hellhound | Troll | Ghoul
4 | Giant, Fire | Fly, Giant | Bugbear
5 | Purple Worm | Scorpion, Giant | Goblin
6 | Fly, Giant | NPC Party:  Bandit | Centaur
7 | Scorpion, Giant | Lion | NPC Party:  Bandit
8 |  Camel | Boar, Wild | NPC Party:  Merchant
9 | Spider, Giant Tarantula | NPC Party:  Merchant | NPC Party:  Pilgrim
10 | NPC Party:  Merchant | Wolf | NPC Party:  Noble
11 | Hawk | Bee, Giant | Dog
12  | NPC Party:  Bandit | Gnoll | Gargoyle*
13 | Ogre | Goblin | Gnoll
14 | Griffon | Blink Dog | Ogre
15 | Gnoll | Wolf, Dire | Minotaur
16 | Dragon, Red | Giant, Hill | Vampire*


| 2d8 | Jungle | Mountains or Hills | Ocean
|:------:|--------|--------------------|----------|
2 | Dragon, Green | Dragon, White | Dragon, Sea
3 | NPC Party:  Bandit | Roc	(1d6:	1-3 Large, 4-5 Huge, 6 Giant) | Hydra
4 | Goblin | Displacer | Whale, Sperm
5 | Hobgoblin | Lycanthrope, Werewolf* | Crocodile, Giant
6 | Centipede, Giant | Mountain Lion | Crab, Giant
7 | Snake, Giant Python | Wolf | Whale, Killer
8 | Elephant | Spider, Giant Crab | Octopus, Giant
9 | Antelope | Hawk | Shark, Mako
10 | Jaguar | Orc | NPC Party:  Merchant
11 | Stirge | Bat, Giant | NPC Party:  Buccaneer (Pirate)
12 | Beetle, Giant Tiger | Hawk, Giant | Shark, Bull
13 | Caecilia, Giant | Giant, Hill | Roc (1d8: 1-5 Huge, 6-8 Giant)
14 | Shadow* | Chimera | Shark, Great White
15 | NPC Party:  Merchant | Wolf, Dire | Mermaid
16 | Lycanthrope, Weretiger* | Dragon, Red | Sea Serpent

| 2d8 | River or Riverside | Swamp/Marsh | Woods or Forest |
|:---:|-------------|-------------|-------------|
| 2 | Dragon, Black | Dragon, Black | Dragon, Green |
| 3 | Fish, Giant Piranha | Shadow* | Unicorn |
| 4 | Stirge | Troll | Treant
| 5 | Fish, Giant Bass | Lizard, Giant Draco | Orc
| 6 | NPC Party:  Merchant | Centipede, Giant | Boar, Wild
| 7 | Lizardman | Leech, Giant | Bear, Black
| 8 | Crocodile | Lizardman | Hawk, Giant
| 9 | Frog, Giant | Crocodile | Antelope
| 10 | Fish, Giant Catfish | Stirge | Wolf
| 11 | NPC Party:  Buccaneer | Orc | Ogre
| 12 | Troll | Toad, Giant | Bear, Grizzly
| 13 | Jaguar | Troglodyte | Wolf, Dire
| 14 | Nixie | Blood Rose | Giant, Hill
| 15 | Water Termite, Giant | Hangman Tree | Owlbear
| 16 | Dragon, Green | Basilisk | Unicorn




Hex sizes
---------

Water hexes
-----------

Ocean exploration
-----------------

Other uses
----------

Forgotten places
================

Ruins
-----

Relics
------

Remains
-------

Vestiges
--------

Remnants
--------

Refuse
------

Wrecks
------

Skeletons
---------

Antiques
--------

Artifacts
---------

Magic statues
-------------

Lairs
=====

Dungeons
--------

Caves
-----

Burrows
-------

Camp
----

Dwelling
--------

Shipwreck
---------

Ledge
-----

Crevasse
--------

Paths & streams
===============

Orientation
-----------

Rivers
------

Roads
-----

Islands
=======

Type
----

Element
-------

Provisions
----------

Volcano
-------

Castles
=======

Condition
---------

Invader
-------

Abandoned
---------

Garrison
--------

Leader
------

Followers
---------

Name
----

Churches
========

Temple
------

Location
--------

Leadership
----------

Wealth
------

Shrines
-------

Deity
-----

### Appearance

| d12 | DEITY APPEARANCE            |
|:---:|-----------------------------|
|   1 | Concept                     |
|   2 | Force                       |
|   3 | Element                     |
|   4 | Plant                       |
|   5 | Animal                      |
|   6 | Person                      |
|   7 | Monster                     |
|   8 | Place                       |
|   9 | Star                        |
|  10 | Object                      |
|  11 | Unknown                     |
|  12 | Multiple, re-roll 1d4 times |

| d20 | CONCEPT DEITY          |
|:---:|------------------------|
|   1 | Love                   |
|   2 | Freedom                |
|   3 | Piety                  |
|   4 | Hunger                 |
|   5 | Death                  |
|   6 | Strangeness            |
|   7 | Consciousness          |
|   8 | Mathematics            |
|   9 | Life                   |
|  10 | Time                   |
|  11 | Luck                   |
|  12 | Art                    |
|  13 | Justice                |
|  14 | Weather                |
|  15 |                        |
|  16 |                        |
|  17 |                        |
|  18 |                        |
|  19 |                        |
|  20 |                        |

| d20 | FORCE DEITY            |
|:---:|------------------------|
|   1 | Light                  |
|   2 | Magnetism              |
|   3 | Electricity            |
|   4 | Gravity                |
|   5 | Radiation              |
|   6 | Magic                  |
|   7 | Thought                |
|   8 | Heat                   |
|   9 | Cold                   |
|  10 | Wind                   |
|  11 | Waves                  |
|  12 | Laser                  |
|  13 |                        |
|  14 |                        |
|  15 |                        |
|  16 |                        |
|  17 |                        |
|  18 |                        |
|  19 |                        |
|  20 |                        |

| d20 | ELEMENT DEITY          |
|:---:|------------------------|
|   1 | Rock                   |
|   2 | Goo                    |
|   3 | Water                  |
|   4 | Fire                   |
|   5 | Acid                   |
|   6 | Air                    |
|   7 | Crystal                |
|   8 | Bone                   |
|   9 | Metal                  |
|  10 | Ice                    |
|  11 | Oil                    |
|  12 | Wood                   |
|  13 | Ash                    |
|  14 | Magma                  |
|  15 | Glass                  |
|  16 | Plastic                |
|  17 | Fabric                 |
|  18 |                        |
|  19 |                        |
|  20 |                        |

| d20 | PLANT DEITY            |
|:---:|------------------------|
|   1 | Cactus                 |
|   2 | Forest                 |
|   3 | Grass patch            |
|   4 | Giant turnip           |
|   5 | Carnivorous plant      |
|   6 | Purple mushroom        |
|   7 | Palm tree              |
|   8 | Orchid                 |
|   9 | Vine                   |
|  10 | Heap of seeds          |
|  11 | Dead leaf              |
|  12 | Hollow oak             |
|  13 | Crooked fir            |
|  14 | Bonsaï                 |
|  15 | Potted tulip           |
|  16 | Datura                 |
|  17 | Lone branch            |
|  18 | Silver apple           |
|  19 | Alga                   |
|  20 | Bamboo shoot           |

| d20 | ANIMAL DEITY           |
|:---:|------------------------|
|   1 |                        |
|   2 |                        |
|   3 |                        |
|   4 |                        |
|   5 |                        |
|   6 |                        |
|   7 |                        |
|   8 |                        |
|   9 |                        |
|  10 |                        |
|  11 |                        |
|  12 |                        |
|  13 |                        |
|  14 |                        |
|  15 |                        |
|  16 |                        |
|  17 |                        |
|  18 |                        |
|  19 |                        |
|  20 |                        |

| d20 | PERSON DEITY           |
|:---:|------------------------|
|   1 | Mother                 |
|   2 | Pirate                 |
|   3 | Old man                |
|   4 | Empress                |
|   5 | Baby                   |
|   6 | Farmer                 |
|   7 | Scholar                |
|   8 | Teacher                |
|   9 | Embryo                 |
|  10 | Prostitute             |
|  11 | Beggar                 |
|  12 | Blacksmith             |
|  13 | Twins                  |
|  14 | Child                  |
|  15 | Artist                 |
|  16 | Baker                  |
|  17 | Maid                   |
|  18 | Druid                  |
|  19 | Fisherman              |
|  20 | Oneself                |

| d20 | MONSTER DEITY          |
|:---:|------------------------|
|   1 |                        |
|   2 |                        |
|   3 |                        |
|   4 |                        |
|   5 |                        |
|   6 |                        |
|   7 |                        |
|   8 |                        |
|   9 |                        |
|  10 |                        |
|  11 |                        |
|  12 |                        |
|  13 |                        |
|  14 |                        |
|  15 |                        |
|  16 |                        |
|  17 |                        |
|  18 |                        |
|  19 |                        |
|  20 |                        |

| d20 | PLACE DEITY            |
|:---:|------------------------|
|   1 |                        |
|   2 |                        |
|   3 |                        |
|   4 |                        |
|   5 |                        |
|   6 |                        |
|   7 |                        |
|   8 |                        |
|   9 |                        |
|  10 |                        |
|  11 |                        |
|  12 |                        |
|  13 |                        |
|  14 |                        |
|  15 |                        |
|  16 |                        |
|  17 |                        |
|  18 |                        |
|  19 |                        |
|  20 |                        |

| d20 | STAR DEITY             |
|:---:|------------------------|
|   1 |                        |
|   2 |                        |
|   3 |                        |
|   4 |                        |
|   5 |                        |
|   6 |                        |
|   7 |                        |
|   8 |                        |
|   9 |                        |
|  10 |                        |
|  11 |                        |
|  12 |                        |
|  13 |                        |
|  14 |                        |
|  15 |                        |
|  16 |                        |
|  17 |                        |
|  18 |                        |
|  19 |                        |
|  20 |                        |

| d20 | OBJECT DEITY           |
|:---:|------------------------|
|   1 |                        |
|   2 |                        |
|   3 |                        |
|   4 |                        |
|   5 |                        |
|   6 |                        |
|   7 |                        |
|   8 |                        |
|   9 |                        |
|  10 |                        |
|  11 |                        |
|  12 |                        |
|  13 |                        |
|  14 |                        |
|  15 |                        |
|  16 |                        |
|  17 |                        |
|  18 |                        |
|  19 |                        |
|  20 |                        |

| d20 | UNKNOWN DEITY               |
|:---:|-----------------------------|
|   1 | No appearance               |
|   2 | Ineffable                   |
|   3 | Unknowable                  |
|   4 | Death upon perception       |
|   5 | Cannot be remembered        |
|   6 | Polymorph                   |
|   7 | Different for each believer |
|   8 | Cannot be grasped           |
|   9 | Nobody saw it yet           |
|  10 | Testimonies have been lost  |
|  11 |                             |
|  12 |                             |
|  13 |                             |
|  14 |                             |
|  15 |                             |
|  16 |                             |
|  17 |                             |
|  18 |                             |
|  19 |                             |
|  20 |                             |

### Domain

### Name

### Powers

### Relationships

Name
----

Defiling
--------

Protection
----------

Ceremonies
----------

Hamlets
=======

Leader
------

Streets
-------

Shops
-----

Population
----------

Technological level
-------------------

Government
----------

Walls
-----

Name
----

Notes
-----

This is a document Jed McClures originally created in support of his gaming group. The philosophy of randomized tables like these, is that the GM is also playing a game, one that requires skill and creativity.  Creating a narrative out of random results on the fly is what separates the good from the great.  But as always, if the dice and tables result in something that is too boring or too brutal for your players, then use your power & creativity as the GM keep things fun.

In Jeds original document much of the material and artwork contained within was taken directly from Judges Guild publications. 

For this Wilderness Hexplore Revamped edition the editors substituted those tables with useful stuff that's been published under OGL in the last years - within the OSR scene and elsewhere.

